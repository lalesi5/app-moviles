package com.example.reciclerviews

import android.view.View
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView

class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) { //constructor primario en kotlin junto a : es la herencia y solo se puede heredar de una clase

    val listId = itemView.findViewById<TextView>(R.id.item_id)
    val listTitle = itemView.findViewById<TextView>(R.id.item_title)

}

