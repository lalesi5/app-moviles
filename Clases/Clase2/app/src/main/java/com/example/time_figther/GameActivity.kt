package com.example.time_figther

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class GameActivity : AppCompatActivity() {

    private lateinit var activityTitle: TextView
    private lateinit var playerName: String
    private lateinit var gameScoreTextView: TextView
    private lateinit var  timeLeftTextView: TextView
    private lateinit var tapMeButton: Button

    private lateinit var countDownTimer: CountDownTimer
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000

    private var timeLeft = 10
    private var gameScore = 0

    private var isGameStarted = false

    private var TAG = GameActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        Log.d(TAG, "onCreate called, score is $gameScore")

        activityTitle = findViewById(R.id.game_title)
        gameScoreTextView = findViewById(R.id.score_text_view)
        timeLeftTextView = findViewById(R.id.time_left_text_view)
        tapMeButton = findViewById(R.id.tap_me_button)

        playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME) ?: "Error"
        activityTitle.text = getString(R.string.get_ready_player, playerName)

        tapMeButton.setOnClickListener {
            it.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bounce))
            incrementScore()
        }

        if (savedInstanceState != null) {
            gameScore = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            isGameStarted = savedInstanceState.getBoolean(GAME_STARTED)
            restoreGame()
        } else {
            resetGame()
        }

    }

    /* Guardar las variables*/
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(SCORE_KEY, gameScore)
        outState.putInt(TIME_LEFT_KEY, timeLeft)
        outState.putBoolean(GAME_STARTED, isGameStarted)

        countDownTimer.cancel()

        Log.d(TAG, "onSaveInstanceState: Saving score: $gameScore and timeLeft: $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()
        countDownTimer.cancel()
        Log.d(TAG, "onDestroy called")
    }

    private fun incrementScore() {

        if (!isGameStarted) {
            startGame()
        }
        gameScore ++

        gameScoreTextView.text = getString(R.string.score, gameScore)
    }

    private fun resetGame() {
        gameScore = 0
        gameScoreTextView.text = getString(R.string.score, gameScore)

        timeLeft = 10
        timeLeftTextView.text = getString(R.string.time_left, timeLeft)

        configCountDownTimer()

        isGameStarted = false
    }

    private fun startGame() {
        countDownTimer.start()
        isGameStarted = true
    }

    private fun endGame() {
        Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
        resetGame()
    }

    private fun restoreGame() {
        gameScoreTextView.text = getString(R.string.score, gameScore)
        timeLeftTextView.text = getString(R.string.time_left, timeLeft)


        configCountDownTimer()

        if (isGameStarted) {
            countDownTimer.start()
        }
    }

    private fun configCountDownTimer() {
        countDownTimer = object : CountDownTimer((timeLeft * 1000).toLong(), countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeft)
            }

            override fun onFinish() {
                endGame()
            }
        }
    }

    companion object {
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private const val GAME_STARTED = "GAME_STARTED"
    }
}
