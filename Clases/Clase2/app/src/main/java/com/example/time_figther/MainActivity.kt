package com.example.time_figther

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    private lateinit var startButton: Button
    private lateinit var playerName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton = findViewById(R.id.start_button)
        playerName = findViewById(R.id.player_name_input)
        // Se conecta al boton con el id del boton
        startButton.setOnClickListener { showGameActivity() }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return  true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.about_menu) {
            showInfo()
        }
        return true
    }

    fun showInfo() {
        val dialogTitle = getString(R.string.about, BuildConfig.VERSION_NAME)
        val dialogMessage = getString(R.string.about_message)

        val builder = AlertDialog.Builder(this)
        builder.setTitle(dialogTitle)
        builder.setMessage(dialogMessage)
        builder.create().show()
    }

    private fun showGameActivity(){
        val gameActivityIntent = Intent(this, GameActivity::class.java)

        gameActivityIntent.putExtra(INTENT_PLAYER_NAME, playerName.text.toString())

        // Le pasa el intent que se creo
        startActivity(gameActivityIntent)
    }

    companion object {
        const val INTENT_PLAYER_NAME = "playerName"
    }
}
