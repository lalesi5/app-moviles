class   Singleton private constructor(){
    // esta clase define el singnleton

    var sampleName:String?=""

    init {
        "Este es {$this} singleton".also(::println)
    }

    private object Holder{

        // crea una intancia de singleton en el holder
        val INSTANCE= Singleton()
    }

    companion object {

        // en esta parte declaro el objeto, no tienen ningun constructor
        val instance:Singleton by lazy { Holder.INSTANCE }
    }

}