package patterns

interface CarService {
    fun doService()
}

interface CarServiceDecorator : CarService

class BasicCarService : CarService {
    override fun doService() = println("Realizando revision basica-->ok")
}

class CarWash(private val carService: CarService) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("Lavado de carro-->Ok")
    }
}

class InsideCarCleanup(private val carService: CarService) : CarServiceDecorator {
    override fun doService() {
        carService.doService()
        println("Limpieza interna del carro-->Ok")
    }
}

fun main(args: Array<String>) {
    val carService = InsideCarCleanup(CarWash(BasicCarService()))
    carService.doService()
}